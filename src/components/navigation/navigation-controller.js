export default {
    vuex: {
        getters: {
            markers: state => state.markers,
            parsedCSV: state => state.parsedCSV,
        },
    },
};
