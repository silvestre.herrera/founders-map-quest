export default {
    data() {
        return {
            showMenu: false,
        };
    },

    props: [
        'actions',
        'target',
    ],

    methods: {
        toggleMenu() {
            this.$set('showMenu', !this.showMenu);
        },

        handleAction(action) {
            if (this.target) {
                action(this.target);
            } else {
                action();
            }
        },

        hideMenu() {
            setTimeout(() => {
                this.$set('showMenu', false);
            }, 100);
        },
    },
};
