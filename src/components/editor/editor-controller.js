import { updateCSV, } from 'src/vuex/actions';
import codeMirror from 'codemirror';
import 'codemirror/addon/selection/active-line';

export default {
    replace: false,

    props: [
        'model',
    ],

    ready() {
        this.$nextTick(this.initCodeMirror);
    },

    vuex: {
        actions: {
            updateCSV,
        },
    },

    methods: {
        initCodeMirror() {
            const options = { lineNumbers: true, styleActiveLine: true, lineWrapping: true, };
            const editor = codeMirror(this.$el, options);

            editor.setValue(this.model);

            editor.on('change', () => {
                this.updateCSV(editor.getValue());
            });

            this.$watch('model', (value) => {
                if (value !== editor.getValue()) {
                    editor.setValue(value);
                }
            });
        },
    },
};
