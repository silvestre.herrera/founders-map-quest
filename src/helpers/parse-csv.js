import PapaParse from 'papaparse';

/**
 * Converts CSV into JSON. Implements PapaParse.
 *
 * @param   {String}    csv
 * @param   {String}    delimeter
 * @return  {Promise}   promise
 */
function parser(csv, options = { delimeter: ',', header: true, }) {
    const promise = new Promise((resolve, reject) => {
        PapaParse.parse(csv, {
            header: options.header,
            delimeter: options.delimeter,
            dynamicTyping: true,
            skipEmptyLines: true,
            complete(result) {
                resolve(result.data);
            },
            error(err) {
                reject(err);
            },
        });
    });

    return promise;
}

export default parser;
