/**
 * Function factory to determine if an object's property is true or false
 *
 * @param {string} property The property you want to test
 * @return {function} Anonymous
 */
function isSelected(property) {
    return function (key) {
        return this.locations[0][key][property] === true;
    };
}

export default isSelected;
