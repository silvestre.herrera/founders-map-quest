/**
 * Function factory to modify an object's property
 *
 * @param {string} property The property you want to modify
 * @return {function} Anonymous
 */
function setProperty(property) {
    return function (key) {
        this.locations.forEach((location) => {
            for (const prop of Object.keys(location)) {
                if (location.hasOwnProperty(prop)) {
                    if (typeof location[prop] !== 'object') {
                        break;
                    }
                    location[prop][property] = false;
                }
            }
            location[key][property] = true;
        });
    };
}

export default setProperty;
