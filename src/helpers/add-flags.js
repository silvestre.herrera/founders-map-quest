/**
 * Takes an element from the parsed CSV and adds specific
 * flags that help determine a property's role
 *
 * @param {string} entry The element to modify
 * @return {object} entry The modified element
 */
function addFlags(entry) {
    for (const prop of Object.keys(entry)) {
        if (typeof(entry[prop]) === 'object') {
            break;
        }
        entry[prop] = {
            value: entry[prop],
            isLatitude: (prop.toLowerCase().indexOf('latitude') >= 0),
            isLongitude: (prop.toLowerCase().indexOf('longitude') >= 0),
            isTitle: (prop.toLowerCase().indexOf('name') >= 0) ||
                     (prop.toLowerCase().indexOf('title') >= 0),
        };
    }

    return entry;
}

export default addFlags;
