import navigation from 'components/navigation';
import { load, Map, Marker, } from 'vue-google-maps';

load('AIzaSyAsX2ENXAqu0CLTTRtElQg61pFqrQgyVuU', '3.23', [
    'places',
]);

export default {
    created() {
        if (!this.markers.length) {
            this.$router.go('/review');
        }
    },

    components: {
        Map,
        Marker,
        navigation,
    },

    vuex: {
        getters: {
            markers: state => state.markers,
        },
    },

    computed: {
        center() {
            return this.markers[0].position;
        },
    },
};
