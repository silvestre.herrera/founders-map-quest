import editor from 'components/editor';
import navigation from 'components/navigation';
import { updateCSV, addParsedEntry, clearParsedCSV, } from 'src/vuex/actions';
import parseCSV from 'src/helpers/parse-csv';

export default {
    data() {
        return {
            parserOptions: {
                header: true,
                delimeter: '',
            },
            showOptions: false,
        };
    },

    vuex: {
        getters: {
            csv: state => state.csv,
            parsedCSV: state => state.parsedCSV,
        },

        actions: {
            updateCSV,
            addParsedEntry,
            clearParsedCSV,
        },
    },

    components: {
        editor,
        navigation,
    },

    methods: {
        loadFile() {
            if (!this.$els.csv.files[0]) return;

            const reader = new FileReader();

            reader.onload = () => {
                this.updateCSV(reader.result);
            };

            reader.readAsText(this.$els.csv.files[0]);
        },

        parse() {
            if (!this.csv) return;

            parseCSV(this.csv, this.parserOptions)
                .then((data) => {
                    this.clearParsedCSV();
                    data.forEach(entry => this.addParsedEntry(entry));
                    this.$router.go('/review');
                })
                .catch(error => {
                    throw new Error(error);
                });
        },

        toggleOptions() {
            this.$set('showOptions', !this.showOptions);
        },
    },
};
