import addFlags from 'src/helpers/add-flags';
import isSelected from 'src/helpers/is-selected';
import strings from 'src/helpers/strings';
import setProperty from 'src/helpers/set-property';
import contextMenu from 'components/context-menu';
import navigation from 'components/navigation';
import { addMarker, clearMarkers, } from 'src/vuex/actions';

export default {
    created() {
        if (!this.parsedCSV.length) this.$router.go('/editor');
    },

    data() {
        return {
            locations: [],
            query: '',
            actions: [
                {
                    name: 'Mark as latitude',
                    checkable: true,
                    checked: this.isLatitude,
                    action: this.markAsLatitude,
                },
                {
                    name: 'Mark as longitude',
                    checkable: true,
                    checked: this.isLongitude,
                    action: this.markAsLongitude,
                },
                {
                    name: 'Mark as title',
                    checkable: true,
                    checked: this.isTitle,
                    action: this.markAsTitle,
                },
            ],
        };
    },

    vuex: {
        actions: {
            addMarker,
            clearMarkers,
        },
        getters: {
            parsedCSV: state => state.parsedCSV,
        },
    },

    components: {
        contextMenu,
        navigation,
    },

    computed: {
        hasHeader() {
            if (!this.parsedCSV.length) return false;

            return !Array.isArray(this.parsedCSV[0]);
        },

        headers() {
            const headers = [];

            if (!this.locations || !this.locations.length) {
                return [];
            }
            for (const prop of Object.keys(this.locations[0])) {
                headers.push(prop);
            }

            return headers;
        },

        atLeastOne() {
            return this.locations.filter(location => location.active).length > 0;
        },

        propCount() {
            return Object.keys(this.locations[0]).length;
        },
    },

    methods: {
        markAsLongitude: setProperty('isLongitude'),
        markAsLatitude: setProperty('isLatitude'),
        markAsTitle: setProperty('isTitle'),
        isLatitude: isSelected('isLatitude'),
        isLongitude: isSelected('isLongitude'),
        isTitle: isSelected('isTitle'),

        isURL(str) {
            return strings.isURL.test(str);
        },

        isImageURL(str) {
            return this.isURL(str) && strings.isIMG.test(str.split('.').pop());
        },

        sendToMap() {
            this.clearMarkers();

            this.locations.forEach((location) => {
                if (location.active) {
                    this.addMarker(location);
                }
            });

            if (this.atLeastOne) {
                this.$router.go('/map');
            }
        },
    },

    ready() {
        const locations = [];

        if (!this.parsedCSV || !this.parsedCSV.length) return;

        if (this.hasHeader) {
            this.parsedCSV.forEach(entry => {
                entry = addFlags(entry);
                locations.push(entry);
            });
        } else {
            this.parsedCSV.forEach(entry => {
                let newEntry = {};

                entry.forEach((item, i) => {
                    newEntry[i] = item;
                });

                newEntry = addFlags(newEntry);
                locations.push(newEntry);
            });
        }

        this.$set('locations', locations);
    },
};
