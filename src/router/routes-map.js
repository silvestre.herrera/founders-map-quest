import editor from 'sections/editor';
import index from 'sections/index';
import map from 'sections/map';
import notFound from 'sections/not-found';
import review from 'sections/review';

export default {
    '*': {
        component: notFound,
    },
    '/': {
        component: index,
    },
    '/editor': {
        component: editor,
    },
    '/review': {
        component: review,
    },
    '/map': {
        component: map,
    },
};
