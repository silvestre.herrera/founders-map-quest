/**
 *  Dispatches an ADD_MARKER mutation
 *
 *  @param {Object} marker The marker object to add
 *  @return void
 */
export const addMarker = ({ dispatch, }, marker) => {
    const newMarker = {
        title: null,
        position: {
            lat: null,
            lng: null,
        },
    };

    for (const prop of Object.keys(marker)) {
        if (marker[prop].isLatitude) {
            newMarker.position.lat = marker[prop].value;
        }
        if (marker[prop].isLongitude) {
            newMarker.position.lng = marker[prop].value;
        }
        if (marker[prop].isTitle) {
            newMarker.title = marker[prop].value;
        }
    }

    if (newMarker.position.lat && newMarker.position.lng) {
        dispatch('ADD_MARKER', newMarker);
    }
};

/**
 *  Dispatches a CLEAR_MARKERS mutation
 *
 *  @return void
 */
export const clearMarkers = ({ dispatch, }) => {
    dispatch('CLEAR_MARKERS');
};

/**
 *  Dispatches an UPDATE_CSV mutation
 *
 *  @param {String} data The CSV string
 *  @return void
 */
export const updateCSV = ({ dispatch, }, data) => {
    dispatch('UPDATE_CSV', data);
};

/**
 *  Dispatches an ADD_PARSED_ENTRY mutation
 *
 *  @param {Object} entry The JSON object to add
 *  @return void
 */
export const addParsedEntry = ({ dispatch, }, entry) => {
    dispatch('ADD_PARSED_ENTRY', entry);
};

/**
 *  Dispatches a CLEAR_PARSED_CSV mutation
 *
 *  @return void
 */
export const clearParsedCSV = ({ dispatch, }) => {
    dispatch('CLEAR_PARSED_CSV');
};
