import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const state = {
    markers: [],
    parsedCSV: [],
    csv: '',
};

const mutations = {
    // Markers Mutations
    ADD_MARKER(state, newMarker) {
        state.markers.push(newMarker);
    },

    CLEAR_MARKERS(state) {
        state.markers.splice(0, state.markers.length);
    },

    // CSV Mutations
    UPDATE_CSV(state, data) {
        state.csv = data;
    },

    ADD_PARSED_ENTRY(state, newEntry) {
        state.parsedCSV.push(newEntry);
    },

    CLEAR_PARSED_CSV(state) {
        state.parsedCSV.splice(0, state.parsedCSV.length);
    },
};

export default new Vuex.Store({
    state,
    mutations,
});
