import isSelected from 'src/helpers/is-selected';

describe('Is Selected Helper', () => {
    const data = {
        locations: [
            {
                someProp: {
                    name: 'Some property',
                    isLatitude: false,
                    isLongitude: false,
                },
            },
        ],
    };

    it('Should be able to see if a property is flagged', () => {
        const isLatitude = isSelected('isLatitude');
        expect(isLatitude.call(data, 'someProp')).to.equal(false);
        data.locations[0].someProp.isLatitude = true;
        expect(isLatitude.call(data, 'someProp')).to.equal(true);
    });
});
