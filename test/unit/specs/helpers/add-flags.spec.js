import addFlags from 'src/helpers/add-flags';

describe('Add Flags Helper', () => {
    const data = {
        title: 'Some title',
        latitude: 12345,
        longitude: 12345,
    };

    it('Should convert a String property into an object', () => {
        expect(typeof data.title).to.equal('string');
        addFlags(data);
        expect(typeof data.title).to.equal('object');
        expect(data.title.value).to.contain('Some title');
    });

    it('Should detect which properties correspond to geographical coords', () => {
        addFlags(data);
        expect(typeof data.latitude).to.equal('object');
        expect(data.latitude.isLatitude).to.equal(true);
    });
});
