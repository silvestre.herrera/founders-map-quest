import strings from 'src/helpers/strings';

describe('Strings helpers', () => {
    it('Should determine wether a string is a URL', () => {
        const URL = 'http://www.google.com/';
        expect(strings.isURL.test(URL)).to.equal(true);
    });

    it('Should determine wether a URL points to an image', () => {
        const URL = 'http://www.google.com/';
        const imgURL = 'http://www.google.com/image.png';
        expect(strings.isURL.test(imgURL)).to.equal(true);
        expect(strings.isIMG.test(imgURL.split('.').pop())).to.equal(true);
        expect(strings.isIMG.test(URL.split('.').pop())).to.equal(false);
    });
});
