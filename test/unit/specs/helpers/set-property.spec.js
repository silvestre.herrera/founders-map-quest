import setProperty from 'src/helpers/set-property';

describe('Set Property Helper', () => {
    const data = {
        locations: [
            {
                someProp: {
                    name: 'Some property',
                    isLatitude: false,
                    isLongitude: false,
                },
            },
        ],
    };

    it('Should be able to change the value of an object\'s property', () => {
        const changeLatitude = setProperty('isLatitude');
        expect(data.locations[0].someProp.isLatitude).to.equal(false);
        changeLatitude.call(data, 'someProp');
        expect(data.locations[0].someProp.isLatitude).to.equal(true);
    });
});
