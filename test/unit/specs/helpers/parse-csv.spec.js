import parseCSV from 'src/helpers/parse-csv';

describe('CSV Parser Helper', () => {
    const data = 'Company Name,Founder,Address,Photo,Home Page,Garage Latitude,Garage Longitude\n' +
        'Google,Larry Page & Sergey Brin,1600 Amphitheatre Pkwy Mountain View CA,' +
        'http://www.incomediary.com/wp-content/uploads/2012/02/larry-page-sergey-brin.jpg,' +
        'http://google.com,37.457674,-122.163452\n' +
        'Apple,Steve Jobs & Steve Wozniak,1 Infinite Loop Cupertino CA,' +
        'http://goo.gl/T7k0e3,' +
        'http://apple.com,37.3403188,-122.0581469\n' +
        'Microsoft,Bill Gates,One Microsoft Way Redmond WA,' +
        'http://pictures.topspeed.com/IMG/crop/200701/bill-gates-famous-mu_600x0w.jpg,' +
        'http://microsoft.com,37.472189,-122.190191';

    it('Should return JSON data', () => {
        parseCSV(data)
            .then((result) => {
                expect(result).not.to.be(null);
                expect(typeof result).to.equal('object');
            });

        parseCSV(false)
            .catch((error) => {
                expect(error).not.to.be(null);
            });
    });
});
