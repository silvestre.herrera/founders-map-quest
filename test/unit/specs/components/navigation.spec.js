import Vue from 'vue';
import navigation from 'components/navigation';
import store from 'src/vuex/store';

describe('Navigation Component', () => {
    let vm;

    beforeEach((done) => {
        vm = new Vue({
            template: '<div><navigation v-ref:navigation-component></navigation></div>',

            methods: {
                doSomething() {
                    this.actions[0].name = 'Changed';
                },
            },

            components: {
                navigation,
            },

            store,
        }).$mount();

        vm.$nextTick(() => done());
    });

    it('Should render nav items', () => {
        expect(vm.$el.querySelector('li')).not.to.equal(null);
    });

    it('Should disable sections not ready for viewing', () => {
        const anchor = vm.$el.querySelector('li:last-child a');
        expect(anchor.classList.contains('navigation__item--disabled')).to.equal(true);
    });
});
