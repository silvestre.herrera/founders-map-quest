import Vue from 'vue';
import contextMenu from 'components/context-menu';

describe('Context Menu Component', () => {
    let vm;

    beforeEach((done) => {
        vm = new Vue({
            template: `<div>
                <context-menu v-ref:context-menu-component :actions="actions"></context-menu>
            </div>`,

            data() {
                return {
                    actions: [
                        {
                            name: 'Menu Item',
                            action: this.doSomething,
                            checkable: true,
                            checked() {
                                return true;
                            },
                        },
                    ],
                };
            },

            methods: {
                doSomething() {
                    this.actions[0].name = 'Changed';
                },
            },

            components: {
                contextMenu,
            },
        }).$mount();

        vm.$nextTick(() => done());
    });

    it('Should render the menu items', () => {
        expect(vm.$el.querySelector('li')).not.to.equal(null);
    });

    it('Should run the menu\'s action', () => {
        expect(vm.$el.querySelector('li a').textContent).to.contain('Menu Item');
        vm.$el.querySelector('li a').click();
        vm.$nextTick(() => {
            expect(vm.$el.querySelector('li a').textContent).to.contain('Changed');
        });
    });

    it('Should be able to toggle the menu', () => {
        const contextMenuComponent = vm.$refs.contextMenuComponent;

        expect(contextMenuComponent.showMenu).to.equal(false);
        contextMenuComponent.toggleMenu();
        expect(contextMenuComponent.showMenu).to.equal(true);
    });

    it('Should be able to hide the menu', (done) => {
        const contextMenuComponent = vm.$refs.contextMenuComponent;

        expect(contextMenuComponent.showMenu).to.equal(false);
        contextMenuComponent.toggleMenu();
        expect(contextMenuComponent.showMenu).to.equal(true);
        contextMenuComponent.hideMenu();
        setTimeout(() => {
            expect(contextMenuComponent.showMenu).to.equal(false);
            done();
        }, 150);
    });
});
